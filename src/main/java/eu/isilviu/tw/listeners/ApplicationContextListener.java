package eu.isilviu.tw.listeners;

import eu.isilviu.tw.persistence.DatabaseUtilities;
import eu.isilviu.tw.persistence.H2Connection;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ApplicationContextListener implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(ApplicationContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.info("The application started.");
        DatabaseUtilities.createTables();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        logger.info("The application was stopped.");
        H2Connection.close();
    }
}
