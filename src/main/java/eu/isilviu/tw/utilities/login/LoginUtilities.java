package eu.isilviu.tw.utilities.login;

import eu.isilviu.tw.persistence.PlayersRepository;
import eu.isilviu.tw.security.HashingUtilities;

public class LoginUtilities {
    private static final PlayersRepository PLAYERS_REPOSITORY = new PlayersRepository();

    /**
     * Handles the login logic.
     * First, it checks if the username exists in the database. If it exists, then it queries for its hash.
     * If the password matches with the hash, then the user was successfully logged in.
     * @param username the username
     * @param password the password
     * @return whether the user was successfully authenticated or not
     */
    public static boolean login(String username, String password){
        if(!PLAYERS_REPOSITORY.contains(username))
            return false;

        String hash = PLAYERS_REPOSITORY.getHash(username);
        return HashingUtilities.match(password, hash);
    }
}
