package eu.isilviu.tw.utilities.login;

import eu.isilviu.tw.models.Player;
import eu.isilviu.tw.persistence.PlayersRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The type Login middleware.
 */
public class LoginMiddleware {

    private static final PlayersRepository PLAYERS_REPOSITORY = new PlayersRepository();

    /**
     * Ensures the player is logged in.
     * @param req  the request
     * @param resp the response
     * @return true or false
     * @throws IOException the io exception
     */
    public static boolean ensureLoggedIn(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        var user = getLoggedIn(req);
        if(user == null)
        {
            resp.sendRedirect("/");
            return false;
        }
        return true;
    }

    /**
     * Gets logged in.
     * @param req the request
     * @return the logged in player
     */
    public static Player getLoggedIn(HttpServletRequest req)
    {
        var username = (String)req.getSession(true).getAttribute("username");
        return PLAYERS_REPOSITORY.getPlayer(username);
    }

    /**
     * Finds if a user is logged in.
     * @param req the request
     * @return true or false
     */
    public static boolean isLoggedIn(HttpServletRequest req)
    {
        return getLoggedIn(req) != null;
    }

}
