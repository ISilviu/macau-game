package eu.isilviu.tw.utilities.login;

import java.util.HashSet;
import java.util.Set;

/**
 * Keeps track of the logged in users.
 */
public class LoggedInUsernames {

    private Set<String> loggedInUsernames = new HashSet<>();

    private static LoggedInUsernames instance;

    private LoggedInUsernames(){}

    /**
     * Get logged in usernames.
     *
     * @return the logged in usernames
     */
    public static LoggedInUsernames get(){
        if(instance == null)
            instance = new LoggedInUsernames();
        return instance;
    }

    /**
     * Add boolean.
     *
     * @param username the username
     * @return the boolean
     */
    public boolean add(String username){
        return loggedInUsernames.add(username);
    }

    /**
     * Remove boolean.
     *
     * @param username the username
     * @return the boolean
     */
    public boolean remove(String username){
        return loggedInUsernames.remove(username);
    }

    /**
     * Contains boolean.
     *
     * @param username the username
     * @return the boolean
     */
    public boolean contains(String username){
        return loggedInUsernames.contains(username);
    }
}
