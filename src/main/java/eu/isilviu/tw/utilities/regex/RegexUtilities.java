package eu.isilviu.tw.utilities.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtilities {

    private static final Pattern ROOM_ID_PATTERN = Pattern.compile("^\\/([1-9]+)$");
    private static final Pattern ROOM_PLAYERS_ID_PATTERN = Pattern.compile("/rooms/players/([1-9]+)");

    public static final int NOT_FOUND_ID = -1;

    /**
     * Gets game id.
     *
     * @param url the url
     * @return the game id
     */
    public static int getGameId(String url) {return getId(url, ROOM_ID_PATTERN);}

    /**
     * Get room id in a given url.
     * @param url the url
     * @return the int
     */
    public static int getRoomId(String url){
        return getId(url, ROOM_ID_PATTERN);
    }

    /**
     * Get room players id.
     * @param url the url
     * @return the int
     */
    public static int getRoomPlayersId(String url){
        return getId(url, ROOM_ID_PATTERN);
    }

    /**
     * Contains the actual logic that extracts the id.
     * @param url
     * @param pattern
     * @return
     */
    private static int getId(String url, Pattern pattern){
        int id = -1;
        Matcher matcher = pattern.matcher(url);
        if(matcher.find()){
            id = Integer.parseInt(matcher.group(1));
        }
        return id;
    }
}
