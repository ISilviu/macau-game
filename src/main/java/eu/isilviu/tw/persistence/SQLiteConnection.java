package eu.isilviu.tw.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class that contains convenience methods for interacting with the database connection.
 */
public class SQLiteConnection {

    private static Connection connection;

    /**
     * Getter method that ensures only one connection to the database per program-run.
     * @return the connection
     */
    static Connection get(){
        String url = "jdbc:sqlite:F:\\Homeworks\\Anul III\\web-techonologies\\MacauGame\\database\\Macau.db";
        try {
            if(connection != null) {
                if (!connection.isClosed()) {
                    connection = openConnection(url);
                }
            } else{
                connection = openConnection(url);
            }
        }catch(SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * Requests the closing of the connection.
     */
    public static void requestClose(){
        try {
            if(connection != null){
                if(!connection.isClosed()){
                    connection.close();
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Opens the connection on the specified url.
     * @param url the connection string
     * @return the connection
     * @throws SQLException if the url is improperly specified
     */
    private static Connection openConnection(String url) throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
       return DriverManager.getConnection(url);
    }
}
