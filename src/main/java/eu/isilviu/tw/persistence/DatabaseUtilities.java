package eu.isilviu.tw.persistence;

import org.apache.log4j.Logger;
import org.h2.engine.Database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseUtilities {

    private static final Logger logger = Logger.getLogger(DatabaseUtilities.class);

    private static final String PLAYERS_TABLE_SQL = "create table if not exists players" +
            "    (id IDENTITY not null, " +
            "    username varchar not null, " +
            "    password_hash varchar not null, " +
            "    first_name varchar, " +
            "    last_name varchar, " +
            "    constraint players_pk " +
            "        primary key (id)," +
            "    unique index (username)" +
            "); ";// +
            //"create unique index players_username_uindex on players (username);";

    /**
     * Creates all the tables in the database.
     */
    public static void createTables(){
        final Connection connection = H2Connection.get();
        try {
            Statement statement = connection.createStatement();
            statement.execute(PLAYERS_TABLE_SQL);
            logger.info("The database tables were created");
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }
}
