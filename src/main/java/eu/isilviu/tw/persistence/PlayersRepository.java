package eu.isilviu.tw.persistence;

import eu.isilviu.tw.models.Player;

import java.sql.Connection;
import java.sql.SQLException;

public class PlayersRepository {

    private static final Connection CONNECTION = H2Connection.get();

    /**
     * Adds a player entity to the database.
     * @param player the player
     * @return whether the player was successfully added or not
     */
    public boolean add(Player player){
        String query="INSERT INTO players(username, password_hash, first_name, last_name) VALUES(?, ?, ?, ?)";
        try(
                var statement = CONNECTION.prepareStatement(query);
            ){
            statement.setString(1, player.getUsername());
            statement.setString(2, player.getHash());
            statement.setString(3, player.getFirstName());
            statement.setString(4, player.getLastName());
            statement.executeUpdate();
            return true;
        } catch(SQLException e){
            return false;
        }
    }

    /**
     * Checks whether the database contains the requested username.
     * @param username the username
     * @return true or false
     */
    public boolean contains(String username){
        String query="SELECT first_name, last_name FROM Players WHERE username=?";
        boolean result = false;
        try(
                var statement = CONNECTION.prepareStatement(query);
        ){
            statement.setString(1, username);
            var resultSet = statement.executeQuery();
            result = resultSet.isBeforeFirst();
        } catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Retrieves the hash of a user. If the user was not found, the function return an empty string.
     * @param username the username
     * @return the hash
     */
    public String getHash(String username){
        String query="SELECT password_hash FROM players WHERE username=?";
        String hash = "";
        try(
                var statement = CONNECTION.prepareStatement(query);
        ){
            statement.setString(1, username);
            var resultSet = statement.executeQuery();
            while(resultSet.next()){
                hash = resultSet.getString("password_hash");
            }
        } catch(SQLException e){
            e.printStackTrace();
        }
        return hash;
    }

    /**
     * Queries the database and reconstructs a Player object from the database data.
     * @param username the username
     * @return the Player object
     */
    public Player getPlayer(String username){
        String query="SELECT first_name, last_name, password_hash FROM players WHERE username=?";
        Player player = null;
        try(
                var statement = CONNECTION.prepareStatement(query);
        ){
            statement.setString(1, username);
            var resultSet = statement.executeQuery();
            while(resultSet.next()){
                var hash = resultSet.getString("password_hash");
                var firstName = resultSet.getString("first_name");
                var lastName = resultSet.getString("last_name");
                player = new Player(username, firstName, lastName, hash);
            }
        } catch(SQLException e){
            return player;
        }
        return player;
    }
}
