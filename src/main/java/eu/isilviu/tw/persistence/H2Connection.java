package eu.isilviu.tw.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2Connection{

    private static Connection connection;

    /**
     * Getter method that ensures only one connection to the database per program-run.
     * @return the connection
     */
    public static Connection get()
    {
        final String url = "jdbc:h2:~/database/Macau";
        try {
            if(connection == null || connection.isClosed())
                openConnection(url);
            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * Closes the connection.
     */
    public static void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens the connection on the specified url.
     * @param url the connection string
     * @throws SQLException if the url is improperly specified
     */
    private static void openConnection(String url) throws SQLException {
        if(connection != null && !connection.isClosed())
            return;

        var driver = new org.h2.Driver();
        connection = DriverManager.getConnection(url);
    }
}
