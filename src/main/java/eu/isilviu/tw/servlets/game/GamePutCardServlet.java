package eu.isilviu.tw.servlets.game;

import eu.isilviu.tw.game.components.Card;
import eu.isilviu.tw.game.components.Rank;
import eu.isilviu.tw.game.components.Suit;
import eu.isilviu.tw.game.components.validators.DefaultMacauValidator;
import eu.isilviu.tw.game.exceptions.IncompatibleCardsException;
import eu.isilviu.tw.game.exceptions.InvalidCardException;
import eu.isilviu.tw.game.exceptions.NonStoppingCardException;
import eu.isilviu.tw.game.structure.GamesLobby;
import eu.isilviu.tw.game.structure.utilities.GamesLobbyUtilities;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import eu.isilviu.tw.utilities.regex.RegexUtilities;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GamePutCardServlet", urlPatterns = "/games/cards/put/*")
public class GamePutCardServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(GamePutCardServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)) {
            var username = (String) req.getSession(true).getAttribute("username");
            int gameId = RegexUtilities.getGameId(req.getPathInfo());
            if (gameId != RegexUtilities.NOT_FOUND_ID) {
                if(GamesLobby.get().contains(gameId)){
                    var game = GamesLobby.get().get(gameId);
                    var player = GamesLobbyUtilities.get(game, username);
                    if(player != null){
                        if(player == game.getCurrentPlayer()){
                            if(game.hasCurrentPlayerMadeAction())
                                req.setAttribute( "error_message","You have already done your action. Click on next turn.");
                            else{
                                String rankString = req.getParameter("rank");
                                String suitString = req.getParameter("suit");

                                try {
                                    Rank rank = Rank.valueOf(rankString);
                                    Suit suit = Suit.valueOf(suitString);
                                    Card desiredCard = new Card(suit, rank, DefaultMacauValidator.get());

                                    if(player.getHand().getCards().contains(desiredCard)){
                                        try {
                                            Rank topCardRank = game.getPile().top().getRank();
                                            boolean isPickingCard = topCardRank == Rank.TWO || topCardRank == Rank.THREE || topCardRank == Rank.JOKER;
                                            if(isPickingCard && !game.hadSpecialCardEffect()){
                                                try {
                                                    game.getHandler().tryPut(game.getPile(), player, desiredCard);
                                                    game.setHadSpecialCardEffect(true);
                                                } catch (NonStoppingCardException e){
                                                    if(topCardRank == Rank.TWO || topCardRank == Rank.THREE)
                                                        player.getHand().add(game.getDeck().deal(game.getHandler().getCardsCount(topCardRank)));
                                                    else{
                                                        player.getHand().add(game.getDeck().deal(game.getHandler().getCardsCount(game.getPile().top())));
                                                    }
                                                    game.setHadSpecialCardEffect(true);
                                                }
                                            } else if(isPickingCard && game.hadSpecialCardEffect()){
                                                game.getHandler().put(game.getPile(), player, desiredCard);
                                            } else{
                                                game.getHandler().tryPut(game.getPile(), player, desiredCard);
                                            }
                                            topCardRank = game.getPile().top().getRank();
                                            isPickingCard = topCardRank == Rank.TWO || topCardRank == Rank.THREE || topCardRank == Rank.JOKER;
                                            if(isPickingCard)
                                                game.setHadSpecialCardEffect(false);
                                            game.setHasCurrentPlayerMadeAction(true);
                                        }catch (IncompatibleCardsException e){
                                            req.setAttribute( "error_message","You can't put that card!");
                                        }
                                    }
                                }catch(InvalidCardException | IllegalArgumentException  e){
                                    logger.error(e.getMessage());
                                }
                            }

                        } else{
                            req.setAttribute( "error_message","It's not your turn.");
                        }
                        req.setAttribute("cards", player.getHand().getCards());
                    }
                    req.setAttribute("game_id", gameId);
                    req.setAttribute("top_card", game.getPile().top());
                    req.getRequestDispatcher("/WEB-INF/views/game.jsp").include(req, resp);
                }
            }
        }
    }
}
