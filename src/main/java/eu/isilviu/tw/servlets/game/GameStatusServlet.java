package eu.isilviu.tw.servlets.game;

import eu.isilviu.tw.game.structure.GamesLobby;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import eu.isilviu.tw.utilities.regex.RegexUtilities;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GameStatusServlet", urlPatterns = "/games/status/*")
public class GameStatusServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)){
            int gameId = RegexUtilities.getGameId(req.getPathInfo());
            if(gameId != RegexUtilities.NOT_FOUND_ID){
                if(GamesLobby.get().contains(gameId))
                    resp.getWriter().print(GamesLobby.get().get(gameId).isRunning().toString());
                 else
                    resp.getWriter().print(Boolean.toString(false));
                resp.getWriter().close();
            }
        }
    }
}
