package eu.isilviu.tw.servlets.game;

import eu.isilviu.tw.game.components.Rank;
import eu.isilviu.tw.game.exceptions.InsufficientCardsException;
import eu.isilviu.tw.game.structure.GamesLobby;
import eu.isilviu.tw.game.structure.utilities.GamesLobbyUtilities;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import eu.isilviu.tw.utilities.regex.RegexUtilities;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "PickCardServlet", urlPatterns = "/games/cards/pick/*")
public class PickCardServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)){
            int gameId = RegexUtilities.getGameId(req.getPathInfo());
            if(gameId != RegexUtilities.NOT_FOUND_ID){
                if(GamesLobby.get().contains(gameId)){
                    var username = (String) req.getSession(true).getAttribute("username");
                    var game = GamesLobby.get().get(gameId);
                    var player = GamesLobbyUtilities.get(game, username);
                    if(player != null){
                        if(player == game.getCurrentPlayer()){
                            if(game.hasCurrentPlayerMadeAction())
                                req.setAttribute( "error_message","You have already done your action. Click on next turn.");
                            else{
                                Rank topCardRank = game.getPile().top().getRank();
                                boolean isPickingCard = topCardRank == Rank.TWO || topCardRank == Rank.THREE || topCardRank == Rank.JOKER;
                                if(isPickingCard && !game.hadSpecialCardEffect()){
                                    if(topCardRank == Rank.TWO || topCardRank == Rank.THREE)
                                        player.getHand().add(game.getDeck().deal(game.getHandler().getCardsCount(topCardRank)));
                                    else{
                                        player.getHand().add(game.getDeck().deal(game.getHandler().getCardsCount(game.getPile().top())));
                                    }
                                    game.setHadSpecialCardEffect(true);
                                }else{
                                    boolean hasAnyCompatibleCard =
                                            player.getHand().getCards().stream().
                                                    anyMatch(card -> game.getHandler().areCompatible(card, game.getPile().top()));
                                    if(hasAnyCompatibleCard)
                                        req.setAttribute( "error_message","You have a compatible card in your hand. Use it!");
                                    else{
                                        try {
                                            game.getHandler().deal(game.getDeck().deal(), player);
                                        }catch(InsufficientCardsException e){
                                            game.getDeck().add(game.getPile().dealAllButFirst());
                                            game.getHandler().deal(game.getDeck().deal(), player);
                                        }
                                    }
                                }
                                game.setHasCurrentPlayerMadeAction(true);
                            }
                        } else{
                            req.setAttribute( "error_message","It's not your turn.");
                        }
                        req.setAttribute("cards", player.getHand().getCards());
                    }
                    req.setAttribute("game_id", gameId);
                    req.setAttribute("top_card", game.getPile().top());
                    req.getRequestDispatcher("/WEB-INF/views/game.jsp").include(req, resp);
                }
            }
        }
    }
}
