package eu.isilviu.tw.servlets.game;

import eu.isilviu.tw.game.components.logichandlers.MacauLogicHandler;
import eu.isilviu.tw.game.components.providers.MacauCompatibilityProvider;
import eu.isilviu.tw.game.components.suppliers.DefaultCardsSupplier;
import eu.isilviu.tw.game.components.suppliers.SpecialCardsSupplier;
import eu.isilviu.tw.game.components.validators.DefaultMacauValidator;
import eu.isilviu.tw.game.structure.Game;
import eu.isilviu.tw.game.structure.GamesLobby;
import eu.isilviu.tw.game.structure.Lobby;
import eu.isilviu.tw.game.structure.Room;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import eu.isilviu.tw.utilities.regex.RegexUtilities;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(name = "GameStartServlet", urlPatterns = "/games/start/*")
public class GameStartServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(GameStartServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)){
            int roomId = RegexUtilities.getRoomId(req.getPathInfo());
            if(roomId != RegexUtilities.NOT_FOUND_ID){
                Room room = Lobby.get().getGameRooms().stream().filter(r->r.getId() == roomId).findAny().orElse(null);
                if(room != null && room.getPlayers().size() >= GamesLobby.MINIMUM_NUMBER_OF_PLAYERS){
                    if(!GamesLobby.get().contains(room.getId()))
                        configureNewGame(room);
                    else if (GamesLobby.get().contains(room.getId()) &&
                            !GamesLobby.get().get(room.getId()).isRunning()) {
                       GamesLobby.get().remove(roomId);
                       configureNewGame(room);
                   }
                    logger.info(String.format("The game %d has started.", roomId));
                    resp.sendRedirect(String.format("/games/cards/%d", room.getId()));
                }
            }
        }
    }

    private void configureNewGame(Room room){
        var game = new Game(
                room,
                new DefaultCardsSupplier(),
                new MacauLogicHandler(
                        Map.ofEntries(
                                Map.entry("TWO", 2),
                                Map.entry("THREE", 3),
                                Map.entry("JOKER_RED", 10),
                                Map.entry("JOKER_BLACK", 5),
                                Map.entry("INITIAL_CARDS", 5)
                        ),
                        new SpecialCardsSupplier(),
                        DefaultMacauValidator.get(),
                        new MacauCompatibilityProvider(new SpecialCardsSupplier())
                ));
        GamesLobby.get().add(game);
        game.run();
        logger.info(String.format("A new game with id %d was created.", room.getId()));
    }
}
