package eu.isilviu.tw.servlets.game;


import eu.isilviu.tw.game.structure.GamesLobby;
import eu.isilviu.tw.game.structure.utilities.GamesLobbyUtilities;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import eu.isilviu.tw.utilities.regex.RegexUtilities;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GamePlayerTurnServlet", urlPatterns = "/games/players/current/*")
public class GamePlayerTurnServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)) {
            int gameId = RegexUtilities.getGameId(req.getPathInfo());
            if (gameId != RegexUtilities.NOT_FOUND_ID) {
                if (GamesLobby.get().contains(gameId)) {
                    var game = GamesLobby.get().get(gameId);
                    var username = (String) req.getSession(true).getAttribute("username");
                    var player = GamesLobbyUtilities.get(game, username);
                    if(player != null){
                        if(player == game.getCurrentPlayer())
                            resp.getWriter().write(Boolean.toString(true));
                        else
                            resp.getWriter().write(Boolean.toString(false));
                        resp.getWriter().close();
                    }
                }
            }
        }
    }
}
