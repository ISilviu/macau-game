package eu.isilviu.tw.servlets.game;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.isilviu.tw.game.structure.GamesLobby;
import eu.isilviu.tw.game.structure.utilities.GamesLobbyUtilities;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import eu.isilviu.tw.utilities.regex.RegexUtilities;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GameCardsServlet", urlPatterns = "/games/cards/*")
public class GameCardsServlet extends HttpServlet {

    private static final Gson gsonWithExposedFields = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)){
            var username = (String)req.getSession(true).getAttribute("username");
            int roomId = RegexUtilities.getRoomId(req.getPathInfo());
            if(roomId != RegexUtilities.NOT_FOUND_ID){
                int gameId = roomId;
                if(GamesLobby.get().contains(gameId)){
                    var game = GamesLobby.get().get(gameId);
                    var player = GamesLobbyUtilities.get(game, username);
                    if(player != null){
                        req.setAttribute("game_id", gameId);
                        req.setAttribute("cards", player.getHand().getCards());
                        req.setAttribute("top_card", game.getPile().top());
                        req.getRequestDispatcher("/WEB-INF/views/game.jsp").forward(req, resp);
//                        String json = gsonWithExposedFields.toJson(
//                                player.getHand().getCards());
//                        resp.getWriter().print(json);
//                        resp.getWriter().close();
                    }
                }
            }
        }

    }
}
