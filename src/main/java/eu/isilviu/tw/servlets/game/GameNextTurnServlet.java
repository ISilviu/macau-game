package eu.isilviu.tw.servlets.game;

import eu.isilviu.tw.game.structure.GamesLobby;
import eu.isilviu.tw.game.structure.utilities.GamesLobbyUtilities;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import eu.isilviu.tw.utilities.regex.RegexUtilities;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GameNextTurnServlet", urlPatterns = "/games/turns/next/*")
public class GameNextTurnServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)){
            int gameId = RegexUtilities.getGameId(req.getPathInfo());
            if(gameId != RegexUtilities.NOT_FOUND_ID){
                if(GamesLobby.get().contains(gameId)){
                    var game = GamesLobby.get().get(gameId);
                    var username = (String) req.getSession(true).getAttribute("username");
                    var player = GamesLobbyUtilities.get(game, username);
                    if(player != null){
                        req.setAttribute("cards", game.getCurrentPlayer().getHand().getCards());
                        if(player == game.getCurrentPlayer()){
                            if(game.hasCurrentPlayerMadeAction())
                                game.nextPlayer();
                            else{
                                req.setAttribute( "error_message","You must make an action before skipping your turn. You could try to put a card on the pile, or pick a card from the deck.");
                            }
                        }
                        else{
                            req.setAttribute( "error_message","It's not your turn.");
                        }
                    }
                    req.setAttribute("game_id", gameId);
                    req.setAttribute("top_card", game.getPile().top());
                    req.getRequestDispatcher("/WEB-INF/views/game.jsp").include(req, resp);
                }
            }
        }
    }
}
