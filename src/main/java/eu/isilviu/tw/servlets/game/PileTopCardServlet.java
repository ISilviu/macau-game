package eu.isilviu.tw.servlets.game;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.isilviu.tw.game.structure.GamesLobby;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import eu.isilviu.tw.utilities.regex.RegexUtilities;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "PileTopCardServlet", urlPatterns = "/games/pile/top/*")
public class PileTopCardServlet extends HttpServlet {

    private static final Gson gsonWithExposedFields = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)){
            int gameId = RegexUtilities.getGameId(req.getPathInfo());
            if(gameId != RegexUtilities.NOT_FOUND_ID){
                if(GamesLobby.get().contains(gameId)){
                    var pile = GamesLobby.get().get(gameId).getPile();
                    String json = gsonWithExposedFields.toJson(pile.top());
                    resp.getWriter().print(json);
                    resp.getWriter().close();
                }
            }
        }
    }
}
