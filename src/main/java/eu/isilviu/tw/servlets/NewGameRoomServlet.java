package eu.isilviu.tw.servlets;

import eu.isilviu.tw.game.structure.Lobby;
import eu.isilviu.tw.game.structure.Room;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

@WebServlet(name = "NewGameRoomServlet", urlPatterns = "/newGameRoom")
public class NewGameRoomServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(NewGameRoomServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp))
            req.getRequestDispatcher("/WEB-INF/views/newGameRoom.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)){
            String roomName = req.getParameter("room_name");
            int roomMaximumPlayers = Integer.parseInt(req.getParameter("room_maximum_players"));
            var user = LoginMiddleware.getLoggedIn(req);
            try {
                var room = new Room(user, roomName, roomMaximumPlayers, LocalDateTime.now());
                Lobby.get().add(room);
                logger.info(String.format("The user %s created a new room named %s with %d maximum number of players.", user.getUsername(), roomName, roomMaximumPlayers));
                resp.sendRedirect("/dashboard");
            }catch (InstantiationException e){
                logger.error(String.format("The user %s tried to create a room that would break a room's number of players constraint:\n %s.", user.getUsername(), e.getMessage()));
                doGet(req, resp);
            }
        }
    }
}
