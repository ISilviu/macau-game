package eu.isilviu.tw.servlets;

import eu.isilviu.tw.utilities.login.LoggedInUsernames;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import eu.isilviu.tw.utilities.login.LoginUtilities;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The servlet that handles index page requests.
 */
@WebServlet(name = "IndexServlet", urlPatterns = "/index")
public class IndexServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(IndexServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.isLoggedIn(req)){
            logger.info(String.format("The user %s was redirected to the dashboard because he is already logged in.", req.getSession(true).getAttribute("username")));
            resp.sendRedirect("/dashboard");
            return;
        }
        req.getRequestDispatcher("/WEB-INF/views/index.jsp").include(req, resp);
    }

    /**
     * Handles post method to authenticate users.
     * @param req the request
     * @param resp the response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var username = req.getParameter("username");
        var password = req.getParameter("password");
        if(LoggedInUsernames.get().contains(username)){
            logger.warn(String.format("The user %s tried to log in again, even if he was already logged in.", username));
            req.setAttribute("login_failed_message", "This user is already logged!");
            req.getRequestDispatcher("/WEB-INF/views/index.jsp").include(req, resp);
            return;
        }
        if(LoginUtilities.login(username, password)) {
            LoggedInUsernames.get().add(username);
            req.getSession(true).setAttribute("username", username);
            logger.info(String.format("The user with the username %s was logged in.", username));
            resp.sendRedirect("/dashboard");
        } else {
            logger.warn(String.format("The username %s and the password entered did not match.", username));
            req.setAttribute("login_failed_message", "The username and the password did not match!");
            req.getRequestDispatcher("/WEB-INF/views/index.jsp").include(req, resp);
        }
    }
}
