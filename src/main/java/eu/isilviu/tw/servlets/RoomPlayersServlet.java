package eu.isilviu.tw.servlets;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.isilviu.tw.game.structure.Lobby;
import eu.isilviu.tw.models.Player;
import eu.isilviu.tw.utilities.regex.RegexUtilities;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet(name = "RoomsPlayersServlet", urlPatterns = "/rooms/players/*")
public class RoomPlayersServlet extends HttpServlet {

    private static final Gson gsonWithExposedFields = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    private void getDataWithoutTimestamp(HttpServletRequest req, HttpServletResponse resp, int roomId) throws IOException {
        var desiredRoom = Lobby.get().getGameRooms()
                .stream()
                .filter(r -> r.getId() == roomId)
                .findAny()
                .orElse(null);
        if(desiredRoom != null) {
            resp.getWriter().print(gsonWithExposedFields.toJson(desiredRoom.getPlayers()));
            resp.getWriter().close();
        }
    }

    private void getDataWithTimestamp(HttpServletRequest req, HttpServletResponse resp, int roomId) throws IOException {
        var instant = Instant.ofEpochMilli(Long.parseLong(req.getParameter("timestamp")));
        var localDateTime = LocalDateTime.ofInstant(instant, ZoneId.of("Europe/Bucharest"));
        var desiredRoom = Lobby.get().getGameRooms()
                .stream()
                .filter(r -> r.getId() == roomId)
                .findAny()
                .orElse(null);
        if(desiredRoom != null){
            List<Player> players =
                    desiredRoom.getPlayersToJoinDate().entrySet()
                            .stream()
                            .filter(entrySet->entrySet.getValue().isAfter(localDateTime))
                            .map(Map.Entry::getKey)
                            .collect(Collectors.toList());
            if(!players.isEmpty()){
                String json = gsonWithExposedFields.toJson(players);
                resp.getWriter().print(json);
                resp.getWriter().close();
            }
        }
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int roomId = RegexUtilities.getRoomPlayersId(req.getPathInfo());
        if(roomId == RegexUtilities.NOT_FOUND_ID)
            return;
        if(req.getParameter("timestamp") == null)
            getDataWithoutTimestamp(req, resp, roomId);
        else{
            getDataWithTimestamp(req, resp, roomId);
        }
    }
}
