package eu.isilviu.tw.servlets.api.rooms;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.isilviu.tw.game.structure.Lobby;
import eu.isilviu.tw.game.structure.Room;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "ApiRoomsPlacesServlet", urlPatterns = "/api/rooms/places")
public class RoomsPlacesServlet extends HttpServlet {

    private static final Gson gson = new GsonBuilder().create();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Integer> availablePlaces = Lobby.get().getGameRooms()
                .stream()
                .map(Room::getAvailablePlaces)
                .collect(Collectors.toList());
        if(!availablePlaces.isEmpty()){
            String json = gson.toJson(availablePlaces);
            resp.getWriter().print(json);
            resp.getWriter().close();
        }
    }
}
