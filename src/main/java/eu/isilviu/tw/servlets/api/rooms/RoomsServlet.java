package eu.isilviu.tw.servlets.api.rooms;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.isilviu.tw.game.structure.Lobby;
import eu.isilviu.tw.game.structure.Room;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet(name = "ApiRoomsServlet", urlPatterns = "/api/rooms")
public class RoomsServlet extends HttpServlet {

    private static final Gson gsonWithExposedFields = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var instant = Instant.ofEpochMilli(Long.parseLong(req.getParameter("timestamp")));
        var localDateTime = LocalDateTime.ofInstant(instant, ZoneId.of("Europe/Bucharest"));

        List<Room> rooms = Lobby.get().getGameRooms().
                stream().
                filter(room -> room.getDateTimeCreated().isAfter(localDateTime)).
                collect(Collectors.toList());
        if(!rooms.isEmpty()){
           String json = gsonWithExposedFields.toJson(rooms);
           resp.getWriter().print(json);
           resp.getWriter().close();

       }
    }
}
