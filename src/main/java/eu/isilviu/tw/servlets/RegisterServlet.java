package eu.isilviu.tw.servlets;

import eu.isilviu.tw.models.Player;
import eu.isilviu.tw.persistence.PlayersRepository;
import eu.isilviu.tw.security.HashingUtilities;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * The servlet that handles player registering.
 */
@WebServlet(name = "RegisterServlet", urlPatterns = "/register")
public class RegisterServlet extends HttpServlet {

    private static final PlayersRepository PLAYERS_REPOSITORY = new PlayersRepository();
    private static final Logger logger = Logger.getLogger(RegisterServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/views/register.jsp").forward(req, resp);
    }

    /**
     * Handles the post method to register users.
     * @param req the request
     * @param resp the response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var username = req.getParameter("username");
        var firstName = req.getParameter("first-name");
        var lastName = req.getParameter("last-name");
        var password = req.getParameter("password");
        var player = new Player(username, firstName, lastName, HashingUtilities.generateHash(password));

        boolean wasAdded = PLAYERS_REPOSITORY.add(player);
        PrintWriter out = resp.getWriter();
        resp.setContentType("text/html");
        if(wasAdded){
            logger.info(String.format("A new user with the username %s was registered.", username));
            out.println();
            out.println("<h1> User registered successfully! </h1>");
            out.println("<h1> Redirecting... </h1>");
            out.flush();
            out.println("<meta http-equiv='refresh' content='2;URL= /index'>");
            out.close();
        }
        else{
            logger.warn(String.format("The username %s is already in use.", username));
            out.println("<h1> Username already in use. </h1>");
            out.println("<meta http-equiv='refresh' content='1;URL=/register'>");
            out.close();
        }
    }
}
