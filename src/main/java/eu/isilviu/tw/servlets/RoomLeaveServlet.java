package eu.isilviu.tw.servlets;

import eu.isilviu.tw.game.structure.Lobby;
import eu.isilviu.tw.game.structure.Room;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import eu.isilviu.tw.utilities.regex.RegexUtilities;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RoomLeaveServlet", urlPatterns = "/rooms/leave/*")
public class RoomLeaveServlet  extends HttpServlet {

    private static final Logger logger = Logger.getLogger(RoomLeaveServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)){
            int roomId = RegexUtilities.getRoomPlayersId(req.getPathInfo());
            if(roomId != RegexUtilities.NOT_FOUND_ID){
                var user = LoginMiddleware.getLoggedIn(req);
                Room requiredRoom = Lobby.get().getGameRooms().stream().filter(room -> room.getId()==roomId).findAny().orElse(null);
                if(requiredRoom != null){
                    requiredRoom.remove(player -> player.getUsername().equals(user.getUsername()));
                    logger.info(String.format("The player %s has left the room with the id %d.", user.getUsername(), requiredRoom.getId()));
                    resp.sendRedirect("/dashboard");
                }
            }
        }
    }
}
