package eu.isilviu.tw.servlets;

import eu.isilviu.tw.game.structure.Lobby;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DashboardServlet", urlPatterns = "/dashboard")
public class DashboardServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(DashboardServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp))
        {
            var player = LoginMiddleware.getLoggedIn(req);
            req.setAttribute("full_name", player.getFirstName() + " " + player.getLastName());
            req.setAttribute("lobby", Lobby.get());
            logger.info(String.format("The user %s has been redirected to his dashboard.", player.getUsername()));
            req.getRequestDispatcher("/WEB-INF/views/dashboard.jsp").forward(req, resp);
        }
    }
}
