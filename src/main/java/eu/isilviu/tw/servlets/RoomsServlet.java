package eu.isilviu.tw.servlets;

import eu.isilviu.tw.game.structure.Lobby;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import eu.isilviu.tw.utilities.regex.RegexUtilities;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RoomsServlet", urlPatterns = "/rooms/*")
public class RoomsServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(RoomsServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)){
            int roomId = RegexUtilities.getRoomId(req.getPathInfo());
            if(roomId != RegexUtilities.NOT_FOUND_ID){
                var desiredRoom = Lobby.get().getGameRooms().stream().filter(room -> room.getId() == roomId).findAny().orElse(null);
                if(desiredRoom != null){
                    var currentPlayer = LoginMiddleware.getLoggedIn(req);
                    if(desiredRoom.getPlayers().stream().noneMatch(player -> player.getUsername().equals(currentPlayer.getUsername())))
                        desiredRoom.add(LoginMiddleware.getLoggedIn(req));

                    req.setAttribute("room", desiredRoom);
                    logger.info(String.format("The user %s has joined the room with the id %d", currentPlayer.getUsername(), desiredRoom.getId()));
                    req.getRequestDispatcher("/WEB-INF/views/room.jsp").include(req, resp);
                }
            }
        }
    }
}
