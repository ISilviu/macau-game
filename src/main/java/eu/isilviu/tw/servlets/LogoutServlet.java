package eu.isilviu.tw.servlets;

import eu.isilviu.tw.utilities.login.LoggedInUsernames;
import eu.isilviu.tw.utilities.login.LoginMiddleware;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LogoutServlet", urlPatterns = "/logout")
public class LogoutServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LogoutServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(LoginMiddleware.ensureLoggedIn(req, resp)){
            var username = (String) req.getSession(true).getAttribute("username");
            LoggedInUsernames.get().remove(username);
            req.getSession(true).removeAttribute("username");
            logger.info(String.format("The user %s has logged out.", username));
            resp.sendRedirect("/index");
        }
    }
}
