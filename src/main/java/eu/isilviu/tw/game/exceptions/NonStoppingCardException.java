package eu.isilviu.tw.game.exceptions;


public class NonStoppingCardException extends IncompatibleCardsException {
    /**
     * Instantiates a new Non stopping card exception.
     * @param message the message
     */
    public NonStoppingCardException(String message) {
        super(message);
    }
}
