package eu.isilviu.tw.game.exceptions;

public class IncompatibleCardsException extends IllegalArgumentException {
    /**
     * Instantiates a new Incompatible cards exception.
     * @param message the message
     */
    public IncompatibleCardsException(String message){
        super(message);
    }
}
