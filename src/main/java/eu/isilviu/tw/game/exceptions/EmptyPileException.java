package eu.isilviu.tw.game.exceptions;

public class EmptyPileException  extends RuntimeException {
    /**
     * Instantiates a new Empty pile exception.
     * @param errorMessage the error message
     */
    public EmptyPileException(String errorMessage){
        super(errorMessage);
    }
}
