package eu.isilviu.tw.game.exceptions;

public class InsufficientCardsException extends RuntimeException {
    /**
     * Instantiates a new Insufficient cards exception.
     * @param errorMessage the error message
     */
    public InsufficientCardsException(String errorMessage) {
        super(errorMessage);
    }
}