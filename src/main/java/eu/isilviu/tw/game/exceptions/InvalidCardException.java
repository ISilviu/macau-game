package eu.isilviu.tw.game.exceptions;

/**
 * Custom exception that is thrown when a card doesn't satisfy the constraints specified by the
 * validator.
 */
public class InvalidCardException extends IllegalStateException {
    /**
     * Instantiates a new Invalid card exception.
     * @param errorMessage the error message
     */
    public InvalidCardException(String errorMessage){
        super(errorMessage);
    }
}

