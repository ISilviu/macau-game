package eu.isilviu.tw.game.components;

import com.google.gson.annotations.Expose;
import eu.isilviu.tw.game.abstractions.ICardValidator;
import eu.isilviu.tw.game.exceptions.InvalidCardException;

import java.util.Objects;

/**
 * The Card type.
 */
public class Card {

    @Expose
    private Suit suit;

    @Expose
    private Rank rank;

    /**
     * Builds the Card object and also validates it using a strategy defined by the validator parameter.
     * @param suit      the suit
     * @param rank      the rank
     * @param validator the validator containing the validation rules
     */
    public Card(Suit suit, Rank rank, ICardValidator validator) {
        if(!validator.isValidCombination(rank, suit))
            throw new InvalidCardException(String.format("The following combination is not valid:\nRank:%s\nSuit:%s", rank.toString(), suit.toString()));
        this.suit = suit;
        this.rank = rank;
    }

    /**
     * Copy constructs a new Card.
     * @param card the card
     */
    public Card(Card card){
        this.rank = card.getRank();
        this.suit = card.getSuit();
    }

    /**
     * Gets suit.
     *
     * @return the suit
     */
    public Suit getSuit() {
        return suit;
    }

    /**
     * Gets rank.
     *
     * @return the rank
     */
    public Rank getRank() {
        return rank;
    }

    @Override
    public String toString() {
        return "Card{" +
                "suit=" + suit +
                ", rank=" + rank +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return suit == card.suit &&
                rank == card.rank;
    }

    @Override
    public int hashCode() {
        return Objects.hash(suit, rank);
    }
}
