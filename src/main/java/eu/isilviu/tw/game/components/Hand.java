package eu.isilviu.tw.game.components;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Hand.
 */
public class Hand {

    private static final int DEFAULT_NUMBER_OF_CARDS = 5;

    private List<Card> cards = new ArrayList<>(DEFAULT_NUMBER_OF_CARDS);

    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);


    /**
     * Adds a card.
     *
     * @param card the card
     */
    public void add(Card card){
        cards.add(card);
    }

    /**
     * Adds a lit of cards.
     *
     * @param cards the cards
     */
    public void add(List<Card> cards){
        this.cards.addAll(cards);
    }

    /**
     * Puts a card.
     * @param card the card
     * @return the card
     */
    public Card put(Card card) {
        Card desiredCard = cards.stream().filter(c->c.equals(card)).findAny().orElse(null);
        if(desiredCard != null){
            Card copy = new Card(desiredCard);
            cards.remove(desiredCard);
            if(cards.isEmpty())
                changeSupport.firePropertyChange("hasCardsLeft", true, false);
            return copy;
        }
        return null;
    }

    /**
     * Puts a card based on its index.
     * @param cardIndex the card index
     * @return the card
     * @throws ArrayIndexOutOfBoundsException if the index is out of bounds
     */
    public Card put(int cardIndex) throws ArrayIndexOutOfBoundsException{
        try {
            Card card = new Card(cards.get(cardIndex));
            cards.remove(cardIndex);
            if(cards.isEmpty())
                changeSupport.firePropertyChange("hasCardsLeft", true, false);
            return card;
        }catch (ArrayIndexOutOfBoundsException e){
            throw e;
        }
    }

    /**
     * Peeks to a card.
     * @param cardIndex the card index
     * @return the card
     * @throws ArrayIndexOutOfBoundsException if the index is out of bounds
     */
    public Card peek(int cardIndex) throws ArrayIndexOutOfBoundsException{
        try {
            return cards.get(cardIndex);
        }catch (ArrayIndexOutOfBoundsException e){
            throw e;
        }
    }

    /**
     * Add property change listener.
     * @param pcl the pcl
     */
    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        changeSupport.addPropertyChangeListener(pcl);
    }

    /**
     * Remove property change listener.
     * @param pcl the pcl
     */
    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        changeSupport.removePropertyChangeListener(pcl);
    }

    /**
     * Get the number of cards.
     * @return the number of cards
     */
    public int getNumberOfCards(){
        return cards.size();
    }

    /**
     * Gets cards.
     * @return the cards
     */
    public List<Card> getCards() {
        return cards;
    }
}
