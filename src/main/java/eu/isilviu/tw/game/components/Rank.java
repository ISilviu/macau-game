package eu.isilviu.tw.game.components;

/**
 * Represents a card's rank.
 */
public enum Rank {
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    ACE,
    JACK,
    QUEEN,
    KING,
    JOKER,
}
