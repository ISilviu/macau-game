package eu.isilviu.tw.game.components.validators;

import eu.isilviu.tw.game.abstractions.ICardValidator;
import eu.isilviu.tw.game.components.Rank;
import eu.isilviu.tw.game.components.Suit;

public class DefaultMacauValidator implements ICardValidator {

    private static DefaultMacauValidator validator;

    private DefaultMacauValidator(){}

    public static DefaultMacauValidator get(){
        if(validator == null){
            validator = new DefaultMacauValidator();
        }
        return validator;
    }

    @Override
    public boolean isValidCombination(Rank rank, Suit suit) {
        if(rank == Rank.JOKER && (suit != Suit.BLACK && suit != Suit.RED))
            return false;
        return rank == Rank.JOKER || (suit != Suit.BLACK && suit != Suit.RED);
    }
}
