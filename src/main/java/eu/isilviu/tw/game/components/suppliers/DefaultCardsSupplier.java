package eu.isilviu.tw.game.components.suppliers;

import eu.isilviu.tw.game.abstractions.ICardsSupplier;
import eu.isilviu.tw.game.components.Card;
import eu.isilviu.tw.game.components.Rank;
import eu.isilviu.tw.game.components.Suit;
import eu.isilviu.tw.game.components.validators.DefaultMacauValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * A class containing the standard Macau game cards.
 */
public class DefaultCardsSupplier implements ICardsSupplier {

    private static final List<Card> CARDS = new ArrayList<>();
    static {
        CARDS.add(new Card( Suit.CLUBS, Rank.TWO		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.CLUBS,Rank.THREE		, DefaultMacauValidator.get() ));
        CARDS.add(new Card( Suit.CLUBS, Rank.FOUR		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.CLUBS, Rank.FIVE		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.CLUBS, Rank.SIX		, DefaultMacauValidator.get()  ));
        CARDS.add(new Card( Suit.CLUBS, Rank.SEVEN		, DefaultMacauValidator.get()  ));
        CARDS.add(new Card( Suit.CLUBS, Rank.EIGHT		, DefaultMacauValidator.get()  ));
        CARDS.add(new Card( Suit.CLUBS, Rank.NINE		, DefaultMacauValidator.get()  ));
        CARDS.add(new Card( Suit.CLUBS, Rank.TEN		, DefaultMacauValidator.get() ));
        CARDS.add(new Card( Suit.CLUBS, Rank.ACE		, DefaultMacauValidator.get() ));
        CARDS.add(new Card( Suit.CLUBS, Rank.JACK		, DefaultMacauValidator.get()  ));
        CARDS.add(new Card( Suit.CLUBS, Rank.QUEEN		, DefaultMacauValidator.get()  ));
        CARDS.add(new Card( Suit.CLUBS, Rank.KING		, DefaultMacauValidator.get()  ));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.TWO		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.THREE	, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.FOUR	, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.FIVE	, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.SIX		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.SEVEN	, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.EIGHT	, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.NINE	, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.TEN		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.ACE		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.JACK	, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.QUEEN	, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.DIAMONDS, Rank.KING	, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.HEARTS, Rank.TWO		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.HEARTS, Rank.THREE		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.HEARTS, Rank.FOUR		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.HEARTS, Rank.FIVE		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.HEARTS, Rank.SIX		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.HEARTS, Rank.SEVEN		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.HEARTS, Rank.EIGHT		, DefaultMacauValidator.get() ));
        CARDS.add(new Card( Suit.HEARTS, Rank.NINE		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.HEARTS, Rank.TEN		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.HEARTS, Rank.ACE		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.HEARTS, Rank.JACK		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.HEARTS, Rank.QUEEN		, DefaultMacauValidator.get() ));
        CARDS.add(new Card( Suit.HEARTS, Rank.KING		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.TWO		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.THREE		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.FOUR		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.FIVE		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.SIX		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.SEVEN		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.EIGHT		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.NINE		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.TEN		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.ACE		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.JACK		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.SPADES, Rank.QUEEN		, DefaultMacauValidator.get()	));
        CARDS.add(new Card( Suit.SPADES, Rank.KING		, DefaultMacauValidator.get()));

        CARDS.add(new Card( Suit.BLACK, Rank.JOKER		, DefaultMacauValidator.get()));
        CARDS.add(new Card( Suit.RED, Rank.JOKER		, DefaultMacauValidator.get()));
    }

    @Override
    public List<Card> getCards() {
        return CARDS;
    }
}
