package eu.isilviu.tw.game.components.logichandlers;

import eu.isilviu.tw.game.abstractions.*;
import eu.isilviu.tw.game.components.*;
import eu.isilviu.tw.game.exceptions.IncompatibleCardsException;
import eu.isilviu.tw.game.exceptions.NonStoppingCardException;
import eu.isilviu.tw.models.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Basic Macau logic handler.
 */
public class MacauLogicHandler implements ILogicHandler {

    private Map<Rank, Integer> SPECIAL_CARDS_NUMBERS = new HashMap<>();
    private Map<Card, Integer> JOKER_CARDS_NUMBERS = new HashMap<>();
    private int initialDealNumberOfCards;
    private ISpecialCardsSupplier specialCardsSupplier;
    private ICompatibilityProvider compatibilityProvider;

    private static final String RED_JOKER_STRING = "JOKER_RED";
    private static final String BLACK_JOKER_STRING = "JOKER_BLACK";
    private static final String INITIAL_CARDS_COUNT = "INITIAL_CARDS";

    /**
     * The constructor initializes the card numbers, i.e. the number of cards to be dealt on a such special card.
     *
     * @param cardsCounts           the map containing the card as string - integer counts
     * @param supplier              the supplier
     * @param validator             the cards validator
     * @param compatibilityProvider the compatibility provider
     * @throws IllegalArgumentException if the provided map contains an non-parsable argument
     */
    public MacauLogicHandler(Map<String, Integer> cardsCounts, ISpecialCardsSupplier supplier, ICardValidator validator, ICompatibilityProvider compatibilityProvider) throws IllegalArgumentException{
        specialCardsSupplier = supplier;
        this.compatibilityProvider = compatibilityProvider;
        cardsCounts.forEach((key, value)->{
            try {
                SPECIAL_CARDS_NUMBERS.put(Rank.valueOf(key), value);
            }catch (IllegalArgumentException e){
                if(RED_JOKER_STRING.equals(key))
                    JOKER_CARDS_NUMBERS.put(new Card(Suit.RED, Rank.JOKER, validator),  value);
                else if(BLACK_JOKER_STRING.equals(key))
                    JOKER_CARDS_NUMBERS.put(new Card(Suit.BLACK, Rank.JOKER, validator), value);
                else if(INITIAL_CARDS_COUNT.equals(key))
                    initialDealNumberOfCards = value;
                else
                    throw new IllegalArgumentException("The provided Map contains unexpected string and could not be parsed.");
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean areCompatible(Card desiredCard, Card topCard) {
        return compatibilityProvider.areCompatible(desiredCard, topCard);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deal(Deck deck, List<Player> players) {
        players.forEach(player -> player.getHand().add(deck.deal(initialDealNumberOfCards)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deal(List<Card> cards, Player player) {
        player.getHand().add(cards);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deal(Card card, Player player) {
        player.getHand().add(card);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tryPut(Pile pile, Player player, Card card) throws IncompatibleCardsException {
        if(card != null){
            Rank topCardRank = pile.top().getRank();
            boolean isPickingCard = topCardRank == Rank.TWO || topCardRank == Rank.THREE || topCardRank == Rank.JOKER;
            if(isPickingCard){
                if(card.getRank() == Rank.FOUR){
                    Card desiredCard = player.getHand().put(card);
                    if(desiredCard != null)
                        pile.add(desiredCard);
                } else
                    throw new NonStoppingCardException("The picking card requires a 4 card to stop it.");
            }
            if(areCompatible(card, pile.top())){
                Card desiredCard = player.getHand().put(card);
                if(desiredCard != null)
                    pile.add(desiredCard);
            }
            else
                throw new IncompatibleCardsException("Incompatible cards.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void put(Pile pile, Player player, Card card) {
        Card desiredCard = player.getHand().put(card);
        if(card != null){
            if(areCompatibleWithEffectApplied(desiredCard, pile.top()))
                pile.add(desiredCard);
        }
    }

    private boolean areCompatibleWithEffectApplied(Card desiredCard, Card topCard){
        return (desiredCard.getRank() == topCard.getRank() || desiredCard.getSuit() == desiredCard.getSuit());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSpecial(Card card){
        return specialCardsSupplier.getCards().contains(card);
    }

    @Override
    public int getCardsCount(Rank rank) {
        return SPECIAL_CARDS_NUMBERS.get(rank);
    }

    @Override
    public int getCardsCount(Card card) {
        return JOKER_CARDS_NUMBERS.get(card);
    }
}
