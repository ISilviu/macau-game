package eu.isilviu.tw.game.components;

/**
 * Represents a card's suit.
 */
public enum Suit {
    DIAMONDS,
    CLUBS,
    HEARTS,
    SPADES,
    RED,
    BLACK,
}
