package eu.isilviu.tw.game.components.suppliers;

import eu.isilviu.tw.game.abstractions.ISpecialCardsSupplier;
import eu.isilviu.tw.game.components.Card;
import eu.isilviu.tw.game.components.Rank;
import eu.isilviu.tw.game.components.Suit;
import eu.isilviu.tw.game.components.validators.DefaultMacauValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Class containing the standard Macau special cards.
 */
public class SpecialCardsSupplier implements ISpecialCardsSupplier {

    private static final List<Card> SPECIAL_CARDS = new ArrayList<>();
    static{
        SPECIAL_CARDS.add(new Card( Suit.CLUBS, Rank.TWO		, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.DIAMONDS, Rank.TWO		, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.HEARTS, Rank.TWO		, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.SPADES, Rank.TWO		, DefaultMacauValidator.get()));

        SPECIAL_CARDS.add(new Card( Suit.CLUBS, Rank.THREE		, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.DIAMONDS, Rank.THREE	, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.HEARTS, Rank.THREE		, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.SPADES, Rank.THREE		, DefaultMacauValidator.get()));

        SPECIAL_CARDS.add(new Card( Suit.CLUBS, Rank.FOUR		, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.DIAMONDS, Rank.FOUR	, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.HEARTS, Rank.FOUR		, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.SPADES, Rank.FOUR		, DefaultMacauValidator.get()));

        SPECIAL_CARDS.add(new Card( Suit.CLUBS, Rank.SEVEN		, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.DIAMONDS, Rank.SEVEN	, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.HEARTS, Rank.SEVEN		, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.SPADES, Rank.SEVEN		, DefaultMacauValidator.get()));

        SPECIAL_CARDS.add(new Card( Suit.BLACK, Rank.JOKER		, DefaultMacauValidator.get()));
        SPECIAL_CARDS.add(new Card( Suit.RED, Rank.JOKER		, DefaultMacauValidator.get()));
    }

    @Override
    public List<Card> getCards() {
        return SPECIAL_CARDS;
    }
}
