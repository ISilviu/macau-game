package eu.isilviu.tw.game.components;

import eu.isilviu.tw.game.abstractions.ICardsSupplier;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The Deck type.
 */
public class Deck extends Pile{
    /**
     * Instantiates a new Deck.
     * @param supplier the cards supplier
     */
    public Deck(ICardsSupplier supplier) {
        this.cards = new ArrayList<>(supplier.getCards());
    }

    /**
     * Shuffles the deck.
     */
    public void shuffle(){
        Collections.shuffle(cards);
    }
}
