package eu.isilviu.tw.game.components;

import eu.isilviu.tw.game.exceptions.EmptyPileException;
import eu.isilviu.tw.game.exceptions.InsufficientCardsException;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Pile.
 */
public class Pile {

    /**
     * The container containing the cards.
     */
    protected List<Card> cards = new ArrayList<>();


    protected static final int FIRST_CARD_INDEX = 0;

    /**
     * Convenience method that checks if there are no cards left in the list.
     *
     * @throws EmptyPileException the empty pile exception
     */
    protected void checkForEmptiness() throws EmptyPileException{
        if(cards.isEmpty())
            throw new EmptyPileException("The pile is empty.");
    }

    /**
     * Check for underflow.
     *
     * @param cardsCount the cards count
     * @throws InsufficientCardsException the insufficient cards exception
     */
    protected void checkForUnderflow(int cardsCount) throws InsufficientCardsException{
        if(cardsCount > cards.size())
            throw new InsufficientCardsException("There are insufficient cards left in the pile.");
    }

    /**
     * Peeks at the last card of the list.
     *
     * @return the last card
     * @throws EmptyPileException if the list was empty
     */
    protected Card getLastCard() throws EmptyPileException{
        checkForEmptiness();
        final int LAST_CARD_INDEX = cards.size() - 1;
        return cards.get(LAST_CARD_INDEX);
    }

    /**
     * Stores a card in the list.
     *
     * @param card the card to be added
     */
    public void add(Card card){
        cards.add(card);
    }

    /**
     * Adds a list of cards to the internal list of cards.
     *
     * @param cards the list of cards to be added
     */
    public void add(List<Card> cards){
        this.cards.addAll(cards);
    }

    /**
     * Deals a card.
     * @return the card
     * @throws EmptyPileException if the pile was empty
     */
    public Card deal() throws EmptyPileException{
        checkForEmptiness();
        Card lastCard = new Card(getLastCard());
        cards.remove(lastCard);
        return lastCard;
    }

    /**
     * Deals a list of cards.
     *
     * @param numberOfCards the number of cards
     * @return the list of cards
     * @throws EmptyPileException         if the pile was empty
     * @throws InsufficientCardsException if there were insufficient cards in the pile
     */
    public List<Card> deal(int numberOfCards)throws EmptyPileException, InsufficientCardsException {
        checkForEmptiness();
        checkForUnderflow(numberOfCards);
        List<Card> dealtCards = new ArrayList<>(cards.subList(cards.size() - numberOfCards, cards.size()));
        cards.subList(cards.size() - numberOfCards, cards.size()).clear();
        return dealtCards;
    }

    /**
     * Checks if the Pile is empty.
     * @return true or false
     */
    public boolean isEmpty(){
        return cards.isEmpty();
    }

    /**
     * Gets the size of the Pile.
     * @return the size of the Pile
     */
    public int size(){
        return cards.size();
    }

    /**
     * Retrieves all cards but the first.
     * @return the list containing the cards
     * @throws EmptyPileException if the list of cards is empty
     */
    public List<Card> dealAllButFirst() throws EmptyPileException{
        checkForEmptiness();
        final int LAST_CARD_INDEX = cards.size() - 1;
        List<Card> requestedCards = new ArrayList<>(cards.subList(FIRST_CARD_INDEX, LAST_CARD_INDEX));
        cards.removeAll(cards.subList(FIRST_CARD_INDEX, LAST_CARD_INDEX));
        return requestedCards;
    }

    /**
     * Peeks the top card of the pile.
     * @return the top card
     * @throws EmptyPileException if the list was empty
     */
    public Card top() throws EmptyPileException{
        return getLastCard();
    }
}
