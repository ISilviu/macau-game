package eu.isilviu.tw.game.components.providers;

import eu.isilviu.tw.game.abstractions.ICompatibilityProvider;
import eu.isilviu.tw.game.abstractions.ISpecialCardsSupplier;
import eu.isilviu.tw.game.components.Card;
import eu.isilviu.tw.game.components.Rank;

import java.util.ArrayList;
import java.util.List;

/**
 * {@inheritDoc}
 */
public class MacauCompatibilityProvider implements ICompatibilityProvider {

    private List<Card> specialCards;

    /**
     * The constructor initializes with a special cards supplier and copies them in an internal container.
     * @param supplier the special cards supplier
     */
    public MacauCompatibilityProvider(ISpecialCardsSupplier supplier){
        specialCards = new ArrayList<>(supplier.getCards());
    }

    /**
     *{@inheritDoc}
     */
    @Override
    public boolean areCompatible(Card desiredCard, Card topCard) {
        Rank topCardRank = topCard.getRank();
        boolean isPickingCard = topCardRank == Rank.TWO || topCardRank == Rank.THREE || topCardRank == Rank.JOKER;
        if(isPickingCard)
            return desiredCard.getRank() == Rank.FOUR;
        if(specialCards.contains(desiredCard))
            return true;
        return areRegularCardsCompatible(topCard, desiredCard);
    }

    /**
     * Utility method the checks whether two regular cards are compatible.
     * @param desiredCard the desired card/the first card
     * @param topCard the pile top card/the second card
     * @return true or false
     */
    private boolean areRegularCardsCompatible(Card desiredCard, Card topCard){
        return (desiredCard.getRank() == topCard.getRank() || desiredCard.getSuit() == topCard.getSuit());
    }
}
