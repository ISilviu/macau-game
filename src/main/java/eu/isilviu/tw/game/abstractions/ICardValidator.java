package eu.isilviu.tw.game.abstractions;

import eu.isilviu.tw.game.components.Rank;
import eu.isilviu.tw.game.components.Suit;

/**
 * Functional interface that provides validates a rank and suit combination.
 * Depending on the game, one combination may by valid or not.
 */
public interface ICardValidator {
    boolean isValidCombination(Rank rank, Suit suit);
}
