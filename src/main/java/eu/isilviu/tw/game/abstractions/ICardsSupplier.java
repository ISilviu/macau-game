package eu.isilviu.tw.game.abstractions;

import eu.isilviu.tw.game.components.Card;

import java.util.List;

/**
 * Interface that defines the set of cards.
 * This provides flexibility as it's not tied only to Macau games.
 */
public interface ICardsSupplier {
    List<Card> getCards();
}
