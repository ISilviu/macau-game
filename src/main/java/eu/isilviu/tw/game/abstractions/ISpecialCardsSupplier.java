package eu.isilviu.tw.game.abstractions;

/**
 * Syntactic sugar for special cards suppliers.
 */
public interface ISpecialCardsSupplier extends ICardsSupplier {
}
