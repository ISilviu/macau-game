package eu.isilviu.tw.game.abstractions;

import eu.isilviu.tw.game.components.Card;
import eu.isilviu.tw.game.components.Deck;
import eu.isilviu.tw.game.components.Pile;
import eu.isilviu.tw.game.components.Rank;
import eu.isilviu.tw.game.exceptions.IncompatibleCardsException;
import eu.isilviu.tw.models.Player;

import java.util.List;

/**
 * Interface that defines common functions needed in a card game.
 */
public interface ILogicHandler {

    /**
     * Checks whether two cards are compatible or not.
     *
     * @param desiredCard the desired card
     * @param topCard     the second card/the card on the top of the pile
     * @return true or false
     */
    boolean areCompatible(Card desiredCard, Card topCard);

    /**
     * Draws cards from the deck and shares them to the players.
     *
     * @param deck    the deck
     * @param players the list of players
     */
    void deal(Deck deck, List<Player> players);

    /**
     * Deals a list of cards to a player.
     *
     * @param cards  the list of cards
     * @param player the player
     */
    void deal(List<Card> cards, Player player);

    /**
     * Deals a card to a player.
     *
     * @param card   the card
     * @param player the player
     */
    void deal(Card card, Player player);


    /**
     * Takes the card from player's hand and tries to put it on the top of the pile.
     * If the cards are compatible, then it puts the card, otherwise it throws an exception.
     *
     * @param pile   the pile
     * @param player the player
     * @param card   the card to be put
     * @throws IncompatibleCardsException if the cards are incompatible
     */
    void tryPut(Pile pile, Player player, Card card) throws IncompatibleCardsException;

    /**
     * Takes a card from player's hand and puts it on the top of the pile without checking if the cards are compatible,
     *
     * @param pile   the pile
     * @param player the player
     * @param card   the card
     */
    void put(Pile pile, Player player, Card card);

    /**
     * Checks whether a card is special.
     *
     * @param card the card
     * @return true or false
     */
    boolean isSpecial(Card card);

    /**
     * Gets cards count.
     *
     * @param rank the rank
     * @return the cards count
     */
    int getCardsCount(Rank rank);

    /**
     * Gets cards count.
     *
     * @param card the card
     * @return the cards count
     */
    int getCardsCount(Card card);
}
