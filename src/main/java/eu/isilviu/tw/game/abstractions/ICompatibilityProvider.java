package eu.isilviu.tw.game.abstractions;

import eu.isilviu.tw.game.components.Card;

/**
 * Functional interface that provides a game with the compatibility of two cards.
 */
public interface ICompatibilityProvider {
    boolean areCompatible(Card desiredCard, Card topCard);
}
