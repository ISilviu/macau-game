package eu.isilviu.tw.game.structure;

import eu.isilviu.tw.game.abstractions.ICardsSupplier;
import eu.isilviu.tw.game.abstractions.ILogicHandler;
import eu.isilviu.tw.game.abstractions.ISpecialCardsSupplier;
import eu.isilviu.tw.game.components.Deck;
import eu.isilviu.tw.game.components.Pile;
import eu.isilviu.tw.models.Player;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Random;


public class Game implements PropertyChangeListener {

    public enum Status{
        RUNNING,
        PAUSED
    }

    public static final int INVALID_ID = -1;

    private int id;
    private Room room;
    private Status status;

    private Deck deck;
    private Pile pile;
    private ILogicHandler handler;

    private PropertyChangeSupport changeSupport;

    private static final String EMPTY_STRING = "";
    private String winnerName = EMPTY_STRING;

    private boolean isRunning;

    private Random random;

    /**
     * Returns if the current special card effect had its effect.
     * @return true or false
     */
    public boolean hadSpecialCardEffect() {
        return hadSpecialCardEffect;
    }

    /**
     * Sets if the had special card effect.
     * @param hadSpecialCardEffect if the card has its effect
     */
    public void setHadSpecialCardEffect(boolean hadSpecialCardEffect) {
        this.hadSpecialCardEffect = hadSpecialCardEffect;
    }

    private boolean hadSpecialCardEffect = false;

    private Player currentPlayer;
    private int currentPlayerIndex = -1;
    private boolean hasCurrentPlayerMadeAction;

    /**
     * Instantiates a new Game.
     * @param room          the room
     * @param cardsSupplier the cards supplier
     * @param handler       the handler
     */
    public Game(Room room, ICardsSupplier cardsSupplier, ILogicHandler handler){
        this.room = room;
        addSubscribers();
        this.id = room.getId();
        deck = new Deck(cardsSupplier);
        pile = new Pile();
        this.handler = handler;
        this.isRunning = true;
        this.changeSupport = new PropertyChangeSupport(this);
        this.random = new Random();
    }

    /**
     * Removes all the subscribers.
     */
    public void removeSubscribers(){
        room.getPlayers().forEach(player -> player.removePropertyChangeListener(this));
        room.getPlayers().forEach(player -> player.getHand().removePropertyChangeListener(this));
    }

    /**
     * Adds subscribers.
     */
    private void addSubscribers(){
        room.getPlayers().forEach(player -> player.addPropertyChangeListener(this));
        room.getPlayers().forEach(player -> player.getHand().addPropertyChangeListener(this));
    }

    /**
     * Adds a property change listener.
     * @param pcl the pcl
     */
    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        changeSupport.addPropertyChangeListener(pcl);
    }

    /**
     * Removes a property change listener.
     * @param pcl the pcl
     */
    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        changeSupport.removePropertyChangeListener(pcl);
    }

    /**
     * Runs the game.
     */
    public void run(){
        deck.shuffle();
        handler.deal(deck, room.getPlayers());
        while(handler.isSpecial(deck.top())){
            deck.shuffle();
        }
        pile.add(deck.deal());
        var owner = room.getPlayers().stream().filter(player -> player.getUsername().equals(room.getOwner().getUsername())).findAny().orElse(null);
        if(owner != null)
            currentPlayer = owner;
        else
            currentPlayer = room.getPlayers().get(random.nextInt(room.getPlayers().size()));
        hasCurrentPlayerMadeAction = false;
        currentPlayerIndex = room.getPlayers().indexOf(currentPlayer);
    }

    /**
     * Advances to the next player.
     */
    public void nextPlayer(){
        ++currentPlayerIndex;
        if(currentPlayerIndex == room.getPlayers().size())
            currentPlayerIndex = 0;
        currentPlayer = room.getPlayers().get(currentPlayerIndex);
        hasCurrentPlayerMadeAction = false;
    }

    /**
     * Gets room.
     * @return the room
     */
    public Room getRoom() { return room; }

    /**
     * Gets id.
     * @return the id
     */
    public int getId() { return id; }

    /**
     * Gets current player.
     * @return the current player
     */
    public Player getCurrentPlayer() { return currentPlayer; }

    /**
     * Gets pile.
     * @return the pile
     */
    public Pile getPile() { return pile; }

    /**
     * Gets handler.
     * @return the handler
     */
    public ILogicHandler getHandler() { return handler; }

    /**
     * Retrieves if the game is running.
     * @return the boolean
     */
    public Boolean isRunning() { return isRunning; }

    /**
     * Gets the winner name.
     * @return the winner name
     */
    public String getWinnerName() { return winnerName; }

    /**
     * Gets the deck.
     * @return the deck
     */
    public Deck getDeck() { return deck; }

    /**
     * Gets the attribute that indicates whether the current player has done his action.
     * @return the boolean
     */
    public boolean hasCurrentPlayerMadeAction() { return hasCurrentPlayerMadeAction; }

    /**
     * Sets if the current player has made an action.
     * @param hasCurrentPlayerMadeAction the has current player made action
     */
    public void setHasCurrentPlayerMadeAction(boolean hasCurrentPlayerMadeAction) { this.hasCurrentPlayerMadeAction = hasCurrentPlayerMadeAction; }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if(propertyChangeEvent.getPropertyName().equals("hasCardsLeft")){
            boolean hasCardsLeft = (boolean) propertyChangeEvent.getNewValue();
            if(!hasCardsLeft){
                isRunning  = false;
                room.getPlayers().forEach(player -> player.getHand().getCards().clear());
            }
        }
        else if(propertyChangeEvent.getPropertyName().equals("winnerName")){
            String winnerName = (String) propertyChangeEvent.getNewValue();
            if(!winnerName.equals(EMPTY_STRING))
                this.winnerName = winnerName;
        }
        if(!isRunning && !winnerName.equals(EMPTY_STRING))
            changeSupport.firePropertyChange("gameId", INVALID_ID, id);
    }
}
