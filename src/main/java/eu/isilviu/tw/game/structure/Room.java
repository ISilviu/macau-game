package eu.isilviu.tw.game.structure;

import com.google.gson.annotations.Expose;
import eu.isilviu.tw.game.structure.exceptions.GameRoomFullException;
import eu.isilviu.tw.models.Player;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Represents a game room which players can join.
 */
public class Room implements PropertyChangeListener {

    @Expose
    private int id;

    private PropertyChangeSupport changeSupport;

    private List<Player> players = new ArrayList<>(6);

    private Map<Player, LocalDateTime> playersToJoinDate = new HashMap<>();

    @Expose
    private String name;

    private int maximumPlayersCount;

    @Expose
    private Player owner;

    @Expose
    private int availablePlaces;

    private LocalDateTime dateTimeCreated;

    @Expose
    private boolean freeToJoin;

    private static final int MINIMUM_PLAYERS_COUNT = 2;
    private static final int MAXIMUM_PLAYERS_COUNT = 6;

    private static int autoIncrementId = 0;
    /**
     * To be removed!
     * @param freeToJoin
     */
    public void setFreeToJoin(boolean freeToJoin) {
        changeSupport.firePropertyChange("freeToJoin", this.freeToJoin, freeToJoin);
        this.freeToJoin = freeToJoin;
    }

    /**
     * The constructor initializes the room's name, the maximum number of players,
     * the number of available places and states that the room can be joined.
     * @param name the name
     * @param maximumPlayersCount the maximum number of players
     */
    public Room(Player owner, String name, int maximumPlayersCount, LocalDateTime dateTimeCreated) throws InstantiationException {
        if(maximumPlayersCount > MAXIMUM_PLAYERS_COUNT || maximumPlayersCount < MINIMUM_PLAYERS_COUNT)
            throw new InstantiationException(String.format("Unsatisfied constraint on the number of players. Range:[%d, %d], Received: %d", MINIMUM_PLAYERS_COUNT, MAXIMUM_PLAYERS_COUNT, maximumPlayersCount));

        ++autoIncrementId;
        this.owner = owner;
        this.id = autoIncrementId;
        this.name = name;
        this.maximumPlayersCount = maximumPlayersCount;
        this.availablePlaces = maximumPlayersCount;
        this.freeToJoin = true;
        this.dateTimeCreated = dateTimeCreated;
        this.changeSupport = new PropertyChangeSupport(this);
    }

    /**
     * Gets the creation date and time.
     * @return the date and time
     */
    public LocalDateTime getDateTimeCreated() {
        return dateTimeCreated;
    }

    public int getId() { return id; }

    /**
     * Thread-safe method that adds a player to a room.
     * @param player the player
     * @throws GameRoomFullException if the room is full
     */
    public synchronized void add(Player player) throws GameRoomFullException {
        if(players.size() == maximumPlayersCount)
            throw new GameRoomFullException("This game room is full.");

        players.add(player);
        playersToJoinDate.put(player, LocalDateTime.now());
        --availablePlaces;
        if(availablePlaces == 0) {
            changeSupport.firePropertyChange("freeToJoin", this.freeToJoin, false);
            freeToJoin = false;
        }
    }

    public Map<Player, LocalDateTime> getPlayersToJoinDate() {
        return playersToJoinDate;
    }

    /**
     * Thread safe method that removes a player from the room
     * @param predicate the predicate to be used when removing a player
     */
    public synchronized void remove(Predicate<? super Player> predicate){
        boolean wasRemoved = players.removeIf(predicate);
        boolean wasRemovedFromMap = playersToJoinDate.keySet().removeIf(predicate);
        if(wasRemoved && wasRemovedFromMap)
            updateLocalAttributes();
    }

    /**
     * Updates the internal state of the Room after Player removal.
     */
    private void updateLocalAttributes(){
        ++availablePlaces;
        if(availablePlaces > 0){
            changeSupport.firePropertyChange("freeToJoin", this.freeToJoin, true);
            freeToJoin = true;
        }
    }

    /**
     * Thread-safe method that removes a player from the room.
     * @param player the player object
     */
    public synchronized void remove(Player player){
        boolean wasRemoved = players.remove(player);
        boolean wasRemovedFromMap = playersToJoinDate.keySet().remove(player);
        if(wasRemoved && wasRemovedFromMap)
            updateLocalAttributes();
    }

    /**
     * Adds a property change listener.
     * @param pcl the property change listener
     */
    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        changeSupport.addPropertyChangeListener(pcl);
    }

    /**
     * Removes a property change listener.
     * @param pcl the property change listener
     */
    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        changeSupport.removePropertyChangeListener(pcl);
    }

    /**
     * Checks whether the room is free to join.
     * @return true or false
     */
    public synchronized boolean getFreeToJoin() {
        return freeToJoin;
    }

    /**
     * Retrieves the number of available places.
     * @return the number of available places
     */
    public synchronized int getAvailablePlaces(){
        return availablePlaces;
    }

    /**
     * Gets the list of players.
     * @return the list of players
     */
    public synchronized List<Player> getPlayers() {
        return players;
    }

    /**
     * Gets the room's name.
     * @return room's name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the owner of a room.
     * @return the Player object
     */
    public Player getOwner() {
        return owner;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {

    }
}
