package eu.isilviu.tw.game.structure;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * The Lobby class contains all the game rooms.
 */
public class Lobby implements PropertyChangeListener {

    private List<Room> gameRooms = new ArrayList<>();
    private boolean isAtLeastOneRoomJoinable = false;
    private int joinableRoomsCount = 0;

    private static Lobby lobby;

    /**
     * The default private constructor ensures there is one instance of the Lobby class.
     */
    private Lobby(){}

    /**
     * Thread-safe getter for the only instance of the Lobby class.
     * @return the lobby
     */
    public static synchronized Lobby get(){
        if(lobby == null){
            lobby = new Lobby();
        }
        return lobby;
    }

    /**
     * Adds a new game room to the lobby and attaches the lobby as the PropertyChangeListener of the room.
     * @param room the new game room
     */
    public synchronized void add(Room room){
        gameRooms.add(room);
        room.addPropertyChangeListener(this);
        ++joinableRoomsCount;
        isAtLeastOneRoomJoinable = true;
    }

    /**
     * Utility method to check whether the Lobby contains a certain room
     * @param predicate the predicate that contains the actual find logic
     * @return true or false
     */
    public synchronized  boolean contains(Predicate<? super Room> predicate){
        return gameRooms.stream().anyMatch(predicate);
    }

    /**
     * Returns whether the lobby has at least one joinable room.
     * @return true or false
     */
    public boolean getIsAtLeastOneRoomJoinable() {
        return isAtLeastOneRoomJoinable;
    }

    /**
     * Gets all the open game rooms.
     * @return the game rooms
     */
    public List<Room> getGameRooms() {
        return gameRooms;
    }

    /**
     * Triggers when notified of a room's status change and checks whether are any joinable rooms left.
     * @param propertyChangeEvent the event
     */
    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        var isJoinableProperty = (boolean) propertyChangeEvent.getNewValue();
        if(!isJoinableProperty){
            --joinableRoomsCount;
            if(joinableRoomsCount == 0)
                isAtLeastOneRoomJoinable = false;
        }
    }
}
