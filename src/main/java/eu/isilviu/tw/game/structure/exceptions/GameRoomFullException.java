package eu.isilviu.tw.game.structure.exceptions;

/**
 * Custom exception that is thrown when a game room is already full.
 */
public class GameRoomFullException extends RuntimeException {
    /**
     * Instantiates a new Game room full exception.
     * @param errorMessage the error message
     */
    public GameRoomFullException(String errorMessage){
        super(errorMessage);
    }
}
