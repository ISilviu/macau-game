package eu.isilviu.tw.game.structure;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Games lobby.
 */
public class GamesLobby implements PropertyChangeListener {

    /**
     * The minimum number of players for any card game.
     */
    public static final int MINIMUM_NUMBER_OF_PLAYERS = 2;

    private Map<Integer, Game> games = new HashMap<>();

    private static GamesLobby instance;

    /**
     * The default private constructor ensures there is one instance of the Games class.
     */
    private GamesLobby(){}

    /**
     * Thread-safe getter for the only instance of the Games class.
     *
     * @return the lobby
     */
    public static synchronized GamesLobby get(){
        if(instance == null){
            instance = new GamesLobby();
        }
        return instance;
    }


    /**
     * Adds a new game.
     * @param game the game
     * @return the game's id
     */
    public synchronized int add(Game game){
        games.put(game.getId(), game);
        game.addPropertyChangeListener(this);
        return game.getId();
    }

    /**
     * Removes a game.
     * @param gameId the game id
     */
    public synchronized void remove(int gameId){
        if(games.containsKey(gameId)){
            var game = games.get(gameId);
            if(games.containsValue(game)){
                game.removeSubscribers();
                game.removePropertyChangeListener(this);
                games.remove(gameId, game);
            }
        }
    }

    /**
     * Convenience method that checks if a game exists.
     * @param gameId the game id
     * @return true or false
     */
    public synchronized Boolean contains(int gameId){
        return games.containsKey(gameId);
    }

    /**
     * Gets a game based on its id.
     * @param gameId the game id
     * @return the game
     */
    public synchronized Game get(int gameId) {return games.get(gameId);}

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if(propertyChangeEvent.getPropertyName().equals("gameOver")){
            int gameId = (int) propertyChangeEvent.getNewValue();
            if(gameId != Game.INVALID_ID){
                if(games.containsKey(gameId)){
                    var game = games.get(gameId);
                    game.removeSubscribers();
                    games.remove(gameId, game);
                }
            }
        }
    }
}
