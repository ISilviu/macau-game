package eu.isilviu.tw.game.structure.utilities;

import eu.isilviu.tw.game.structure.Game;
import eu.isilviu.tw.models.Player;

public class GamesLobbyUtilities {

    public static Player get(Game game, String username){
        return game.getRoom().getPlayers()
                .stream()
                .filter(player -> player.getUsername().equals(username))
                .findAny()
                .orElse(null);
    }
}
