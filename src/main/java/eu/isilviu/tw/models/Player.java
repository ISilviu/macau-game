package eu.isilviu.tw.models;

import com.google.gson.annotations.Expose;
import eu.isilviu.tw.game.components.Hand;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Objects;

/**
 * The type Player.
 */
public class Player implements PropertyChangeListener{

    private String username;
    private String hash;

    @Expose
    private String firstName;

    @Expose
    private String lastName;

    private static final String EMPTY_STRING = "";

    private Hand hand;

    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    /**
     * Instantiates a new Player.
     * @param username  the username
     * @param firstName the first name
     * @param lastName  the last name
     * @param hash      the hash
     */
    public Player(String username, String firstName, String lastName, String hash) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.hash = hash;
        this.hand = new Hand();
        this.hand.addPropertyChangeListener(this);
    }

    /**
     * Get the player's hand.
     * @return the hand
     */
    public Hand getHand(){return hand;}

    /**
     * Gets first name.
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Gets last name.
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Gets username.
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets hash.
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * Adds a property change listener.
     * @param pcl the pcl
     */
    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        changeSupport.addPropertyChangeListener(pcl);
    }

    /**
     * Removes a property change listener.
     * @param pcl the pcl
     */
    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        changeSupport.removePropertyChangeListener(pcl);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return username.equals(player.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if(propertyChangeEvent.getPropertyName().equals("hasCardsLeft")){
            boolean hasCardsLeft = (boolean) propertyChangeEvent.getNewValue();
            if(!hasCardsLeft){
                changeSupport.firePropertyChange("winnerName", EMPTY_STRING, this.firstName + " " + this.lastName);
            }
        }
    }
}
