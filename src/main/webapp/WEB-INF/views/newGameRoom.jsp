<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../assets/css/form.css">
    <title>Title</title>
</head>
<body>
<div style="background: #f7f7f7; width: 15rem; margin-top: 2rem" class="container">
<form action="newGameRoom" method="post">
    <div class="form-group">
        <label style="display: block; text-align:center; font-family: 'Montserrat', sans-serif">Room name</label>
        <input name="room_name" class="form-control" required="required">
    </div>
    <div class="form-group">
        <label  style="display: block; text-align:center; font-family: 'Montserrat', sans-serif">Maximum players</label>
        <input  name="room_maximum_players" type="number" min="2" max="6" class="form-control" required="required">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-outline-primary" style="display: block; margin: 0 auto;">Add</button>
    </div>
</form>
</div>
</body>
</html>
