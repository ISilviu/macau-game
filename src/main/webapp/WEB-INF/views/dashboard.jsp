<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"  %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <title>Dashboard</title>
</head>
<body>
<div class="container">
    <h1 class="text-center display-3">${full_name}</h1>
    <div class="row">
        <a href="logout" style="display: block; margin: 0 auto;" class="btn btn-outline-danger">Log out</a>
        <a href="newGameRoom" class="btn btn-outline-primary" style="display: block; margin: 0 auto;">New game room</a>
    </div>
    &nbsp;
    <div class="row">
        <div class="col-md-12">
                <table id="rooms_table" class="table table-hover" style="margin-left:auto;margin-right:auto;">
                    <thead class="thead-dark">
                    <tr>
                        <th style="text-align: center" scope="col">#</th>
                        <th style="text-align: center" scope="col">Room name</th>
                        <th style="text-align: center" scope="col">Available places</th>
                        <th style="text-align: center" scope="col">Owner</th>
                        <th style="text-align: center" scope="col">Join</th>
                    </tr>
                    </thead>
                    <c:forEach items="${lobby.gameRooms}" var="room">
                        <c:if test="${room.availablePlaces gt 0}">
                        <tr>
                            <td style="text-align: center"><c:out value="${room.id}"/></td>
                            <td style="text-align: center"><c:out value="${room.name}" /></td>
                            <td style="text-align: center"><c:out value="${room.availablePlaces}" /></td>
                            <td style="text-align: center"><c:out value="${room.owner.firstName} ${room.owner.lastName}"/></td>
                            <td style="text-align: center"><a class="btn btn-outline-dark" href="rooms/${room.id}">Join</a></td>
                        </tr>
                        </c:if>
                    </c:forEach>
                </table>
        </div>
    </div>
</div>
<script>
    timestamp = Date.now();
    function addRow(id, name, availablePlaces, ownerName)
    {
        var row = $("<tr>");
        var cols = "";
        cols += '<td style="text-align: center">' + id + '</td>';
        cols += '<td style="text-align: center">' + name + '</td>';
        cols += '<td style="text-align: center">' + availablePlaces + '</td>';
        cols += '<td style="text-align: center">' + ownerName + '</td>';
        cols += '<td style="text-align: center"><a class="btn btn-outline-dark" href="rooms/' + id + '">Join</a></td>';
        row.append(cols);
        $("#rooms_table").append(row);
    }

    function addOption(roomId, isFreeToJoin)
    {
        console.log(isFreeToJoin);
        if(isFreeToJoin)
        {
            var option = $("<option>" + roomId + "</option>");
            $("#game_room_select").append(option);
        }
    }
    function pollData()
    {
        console.log("polling data ...");
        $.getJSON("/api/rooms?timestamp=" + timestamp, function(data) {
            console.log(data);
            $.each(data, function(index, element) {
                console.log(element);
                addRow(element.id, element.name, element.availablePlaces, element.owner.firstName + " " + element.owner.lastName);
                addOption(element.id, (element.freeToJoin));
            });
            timestamp = Date.now();
        });
    }

    function updateTable(newData)
    {
        $.each(newData, function(index, element){
            $('#rooms_table').find('tr:eq(' + (index + 1) + ')').find('td:eq(2)').html(element);
        });
    }

    function pollDataForTable()
    {
        console.log("updating the table...");
        $.getJSON("/api/rooms/places", function(data){
            console.log(data);
            if(data != null)
                updateTable(data);
        })
    }

    setInterval(pollData, 500);
    setInterval(pollDataForTable, 400);
</script>
</body>
<script src="/assets/js/jquery-3.4.1.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/fontawesome.min.js"></script>
<script src="/assets/js/app.js"></script>
</html>
