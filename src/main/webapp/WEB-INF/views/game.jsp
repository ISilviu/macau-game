<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"  %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <title>Simple Game</title>
</head>
<body>
<div class="container">
    <div style="margin:0 auto; width:10rem" class="row">
        <h3 style="text-align: center; color:#005cbf"> Top Card</h3>
    </div>
    <div style="margin:0 auto; height: auto; width: available" class="row">
        <img id="pile_top_card" class="rounded-sm" style='display: flex;
                justify-content: center; height: 30%; width: 20%' src="/assets/images/${top_card.rank}_${top_card.suit}.png"/>

    </div>
    &nbsp;
    <div class="row">
        <a id="pick_card_button"  href="/games/cards/pick/${game_id}" class="btn btn-secondary">Pick a card</a>
        <a id="next_turn_button"  href="/games/turns/next/${game_id}" class="btn btn-success">Next turn</a>
        <% if(request.getAttribute("error_message") != null)
        { %>
        <div style="float: right" class="alert alert-danger">
            ${error_message}
        </div>
        <% } %>
    </div>
    <hr style="display: block;height: 1px;border: 0;border-top: 1px solid #ccc;margin: 1em 0;padding: 0;"/>
</div>
<c:forEach items="${cards}" var="card">
    <a href="/games/cards/put/${game_id}?rank=${card.rank}&suit=${card.suit}">
        <img style='height: 33%; width: 11%;' src="/assets/images/${card.rank}_${card.suit}.png" alt=""/>
    </a>
</c:forEach>
<script>
    let isGameRunning = true;
    function pollGameStatus()
    {
        console.log("polling if the game has started...");
        $.get("/games/status/" + ${game_id}, function (data, status) {
            console.log('Data is: ' + data);
            if(data ==='false')
            {
                console.log('The game has stopped.');
                $.get("/games/winner/" + ${game_id},function (player, status1) {
                    if(player !== "")
                    {
                        isGameRunning = false;
                        console.log('Alert coming...');
                        alert(player + " has won the game.");
                        if(isGameRunning === false)
                        {
                            window.location.href ="..." ;
                            window.location.replace(window.location.origin + '/rooms/${game_id}');
                        }
                    }
                })
            }
        });
    }

    function updatePileTopCard()
    {
        $.getJSON("/games/pile/top/" + ${game_id}, function (pileTopCard) {
            const path =  "/assets/images/" + pileTopCard.rank + "_" + pileTopCard.suit + ".png";
            $('#pile_top_card').attr("src", path);
        })
    }

    function pollPlayerTurn()
    {
        $.get("/games/players/current/${game_id}", function (isCurrentPlayer) {
            if(isCurrentPlayer === 'true')
            {
                $('#next_turn_button').show();
                $('#pick_card_button').show();
            }
            else if(isCurrentPlayer === 'false')
            {
                $('#next_turn_button').hide();
                $('#pick_card_button').hide();
            }
        })
    }
    setInterval(updatePileTopCard, 200);
    setInterval(pollGameStatus, 500);
    setInterval(pollPlayerTurn, 200);
</script>
</body>
<script src="/assets/js/jquery-3.4.1.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/fontawesome.min.js"></script>
<script src="/assets/js/app.js"></script>
</html>
