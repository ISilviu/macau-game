<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"  %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <title>Room</title>
</head>
<body>
<div class="container">
<%--    <h1 class="text-center display-3">${room_name}</h1>--%>
<%--    <h1 class="text-center display-3">${room.players.size()}</h1>--%>

    <div class="row">
        <a href="/rooms/leave/${room.id}" class="btn btn-outline-danger" style="display: block; margin-top:2rem; margin-left: 5rem; margin-right: auto">Leave Room</a>
        <a id="start_game_button" href="/games/start/${room.id}" class="btn btn-outline-primary" style="display: none; margin-top:2rem; margin-left: auto; margin-right: auto">Start Game</a>
    </div>&nbsp;
    <div class="row">
        <div class="col-md-12">
            <c:if test="${not empty room.players}">
                <table id="players_table" class="table table-striped table-hover" style="margin-left:auto;margin-right:auto;">
                    <thead class="thead-dark">
                    <tr>
                        <th style="text-align: center" scope="col">Players</th>
                    </tr>
                    </thead>
                    <c:forEach items="${room.players}" var="player">
                        <tr>
                            <td style="text-align: center"><c:out value="${player.firstName} ${player.lastName}"/></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>
        </div>
    </div>
</div>
<script>
    timestamp = Date.now();
    function addRow(firstName, lastName)
    {
        var row = $("<tr>");
        var cols = "";
        cols += '<td style="text-align: center">' + firstName + ' ' + lastName + '</td>';
        row.append(cols);
        $("#players_table").append(row);
    }

    function pollData()
    {
        console.log("polling data ...");
        $.getJSON("/rooms/players/" + ${room.id} + "?timestamp=" + timestamp,function (data) {
            console.log(data);
            $.each(data, function(index, element){
                console.log(element);
                addRow(element.firstName, element.lastName);
            });
            timestamp = Date.now();
        });
    }

    function enableStartGameButton()
    {
        console.log("trying to see if there are sufficient players in the room ...");
        $.getJSON("/rooms/players/" + ${room.id},function (data) {
            console.log(data);
            console.log('Room players count: ' + data.length);
            if(data.length > 1)
                $('#start_game_button').css('display', 'block');
            else
                $('#start_game_button').css('display', 'none');
        });
    }

    function pollGameStarted()
    {
        console.log("polling if the game has started...");
        $.get("/games/status/" + ${room.id}, function (data, status) {
            console.log('Data is: ' + data);
            if(data ==='true')
               {
                   console.log('Condition was true');
                   window.location.href ="..." ;
                   window.location.replace(window.location.origin + `/games/start/${room.id}`);
               }
        });
    }

    setInterval(pollData, 400);
    setInterval(pollGameStarted, 200);
    setInterval(enableStartGameButton, 400);
</script>
</body>
<script src="/assets/js/jquery-3.4.1.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/fontawesome.min.js"></script>
<script src="/assets/js/app.js"></script>
</html>
