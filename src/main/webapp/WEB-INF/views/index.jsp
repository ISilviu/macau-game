<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../assets/css/form.css">
    <title>Login</title>
</head>
<body>
<div class="login-form">
    <h1 class="text-center display-4">Macau Game</h1>
    &nbsp;
    <form method="post">
        <h2 class="text-center" id="login-text">Login</h2>
        <div class="form-group">
            <input name="username" type="text" class="form-control" placeholder="Username" required="required">
        </div>
        <div class="form-group">
            <input name="password" type="password" class="form-control" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Log in</button>
        </div>
    </form>
    <p class="text-center"><a href="register">Create an Account</a></p>
    <% if(request.getAttribute("login_failed_message") != null)
    { %>
    <div class="alert alert-danger" role="alert">
        ${login_failed_message}
    </div>
    <% } %>
</div>
</body>
<script src="/assets/js/jquery-3.4.1.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/fontawesome.min.js"></script>
<script src="/assets/js/app.js"></script>
</html>
