package eu.isilviu.tw.game.structure;

import eu.isilviu.tw.models.Player;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class RoomTests {

    private static final Player defaultPlayer = new Player("x","y","z<","w");

    @Test
    public void roomConstructorThatThrows() {
        assertThrows(InstantiationException.class, ()->{
            Room room = new Room(defaultPlayer,"n",20, LocalDateTime.now());
        });
    }

    @Test
    public void roomGetterMethods() throws InstantiationException {
        Room room = new Room(defaultPlayer, "someRoom", 5, LocalDateTime.now());
        assertEquals(room.getName(), "someRoom");
        assertEquals(room.getOwner(), defaultPlayer);
        assertEquals(room.getPlayers().size(), 0);
        assertEquals(room.getAvailablePlaces(), 5);
    }

    @Test
    public void roomAddPlayerMethod() throws InstantiationException {
        Room room = new Room(defaultPlayer, "someRoom", 2, LocalDateTime.now());
        room.add(defaultPlayer);
        assertEquals(room.getAvailablePlaces(),1);
        assertTrue(room.getFreeToJoin());
        room.add(defaultPlayer);
        assertEquals(room.getAvailablePlaces(),0);
        assertFalse(room.getFreeToJoin());
    }

    @Test
    public void roomRemovePlayerUsingPredicateMethod() throws InstantiationException{
        Room room = new Room(defaultPlayer, "someRoom", 2, LocalDateTime.now());
        room.add(defaultPlayer);
        room.add(defaultPlayer);
        room.remove(defaultPlayer);
        assertEquals(room.getAvailablePlaces(), 1);
        assertTrue(room.getFreeToJoin());
        room.remove(defaultPlayer);
        assertEquals(room.getAvailablePlaces(), 2);
        assertTrue(room.getFreeToJoin());
    }

}
