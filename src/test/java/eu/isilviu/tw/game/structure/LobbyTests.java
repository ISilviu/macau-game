package eu.isilviu.tw.game.structure;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;


import eu.isilviu.tw.models.Player;

import java.time.LocalDateTime;

public class LobbyTests {

    @Test
    public void thereShouldBeOneInstanceOfLobby(){
        var firstLobby = Lobby.get();
        var secondLobby = Lobby.get();
        assertEquals(firstLobby, secondLobby);
    }

    @Test
    public void addingOneRoomToTheLobby() throws InstantiationException {
        var lobby = Lobby.get();
        lobby.add(new Room(new Player("sill49", "Silviu","Ifrim","jdsio"),"SomeName", 6, LocalDateTime.now()));
        assertEquals(lobby.getGameRooms().size(), 1);
        assertTrue(lobby.getIsAtLeastOneRoomJoinable());
    }

}
